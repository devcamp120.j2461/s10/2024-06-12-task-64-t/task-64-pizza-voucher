package com.devcamp.pizzavoucher;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class PizzavoucherApplication {

	public static void main(String[] args) {
		SpringApplication.run(PizzavoucherApplication.class, args);
	}

}
