package com.devcamp.pizzavoucher.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.devcamp.pizzavoucher.model.CVoucher;

@Repository
public interface CVoucherRepository extends JpaRepository<CVoucher, Long> {
    
}
