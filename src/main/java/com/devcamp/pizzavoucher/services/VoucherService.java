package com.devcamp.pizzavoucher.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.pizzavoucher.model.CVoucher;
import com.devcamp.pizzavoucher.repository.CVoucherRepository;

@Service
public class VoucherService {
    
    @Autowired
    private CVoucherRepository repository;

    public List<CVoucher> getAllVouchers(){
        return repository.findAll();
    }

    public Optional<CVoucher> getVoucherById(Long id){
        return repository.findById(id);
    }

    public CVoucher createVoucher(CVoucher newVoucher){
        return repository.save(newVoucher);
    }

    public CVoucher udpateVoucher(CVoucher voucher){
        return repository.save(voucher);
    }

    public void deleteVoucher(Long id){
        repository.deleteById(id);
    }

    public void deleteAllVoucher(){
        repository.deleteAll();
    }
}
